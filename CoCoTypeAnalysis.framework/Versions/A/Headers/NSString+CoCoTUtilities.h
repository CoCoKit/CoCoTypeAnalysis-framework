//
//  NSString+CoCoTUtilities.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  将字符串哈希的相关工具方法。
 */
@interface NSString (CoCoTCrypto)
/*!
 *  生成并返回当前字符串的MD5哈希字符串
 *
 *
 *  @return MD5 后的字符串
 */
- (NSString *)coco_stringWithMD5;

/*!
 *  生成并返回当前字符串的MD5摘要字符串
 *
 *  @return 生成的MD5摘要字符串
 */
- (NSString *)coco_stringWithMD5HexDigest;

/*!
 *  生成并返回指定NSData转化成MD5哈希字符串
 *
 *  @param data 要转换成MD5字符串的NSData
 *
 *  @return 转换成MD5的字符串
 */
+ (NSString *)coco_md5StringWithData:(NSData *)data;

@end

/*!
 *  字符串常用工具方法。
 */
@interface NSString (CoCoTNSStringUtils)

/*!
 *  生成并返回指定最大长度后的字符串；如果要转换的字符串的长度超过指定长度，生成字符串后三位以“...”显示
 *  如， “abcdef” maxLen = 4 -> "a..."
 *
 *  @param maxLen 最大长度
 *
 *  @return 复合最大长度的字符串
 */
- (NSString *)coco_stringWithMaxLength:(NSUInteger)maxLen;

/*!
 *  生成并返回去掉URL中的变量参数后的字符串
 *  如“abc.com?type=a” -> "abc.com"
 *
 *  @return 去掉参数后的URL字符串
 */
- (NSString *)coco_urlWithoutParameters;

/*!
 *  生成并返回将当前字符串指定范围替换后的字符串
 *
 *  @param aRange  替换范围
 *  @param aString 替换字符串
 *
 *  @return 替换后的字符串
 */
- (NSString *)coco_stringByReplacingRange:(NSRange)aRange with:(NSString *)aString;

/*!
 *  生成并返回去掉当前字符串中的空格和换行符后的字符串
 *
 *  @return 转换后的字符串
 */
- (NSString *)coco_trimmedString;

/*!
 *  生成并返回将当前字符串转义后的字符串
 *  如，“&amp” -> "&"
 *
 *  @return 转换后的字符串
 */
- (NSString *)coco_htmlDecodedString;

/*!
 *  生成并返回将当前字符串转义后的字符串
 *  如 "&" -> "&amp"
 *
 *  @return 转换后的字符串
 */
- (NSString *)coco_htmlEncodedString;

/*!
 *  encode url
 *
 *  @return encode后的url
 */
- (NSString *)coco_urlEncode;

/*!
 *  decode url
 *
 *  @return decode后的url
 */
- (NSString *)coco_urlDecode;

/*!
 *  取得URL中对应参数的值
 *
 *  @param url       需要解析的URL
 *  @param paramName 需要取得的参数名
 *
 *  @return 返回URL中对应参数名的值
 */
NSString* coco_getParamValueCaseInsensitiveFromUrl(NSString *url, NSString *paramName);

/*!
 *  根据制定的URL，数据参数字典和http方法字符串，生成并返回一个序列化后的URL字符串对象。
 *
 *  @param baseURL    需要序列化的URL
 *  @param params     需要添加的参数
 *  @param httpMethod http方法，e.g. "GET"
 *
 *  @return 序列化后的URL
 */
NSString* coco_serializeURL(NSString *baseURL, NSDictionary *params, NSString *httpMethod);

/*!
 *  根据参数字典，生成URL Query参数字符串
 *
 *  @param params    参数列表
 *
 *  @return query字符串
 */
NSString* coco_queryStringWithParameters(NSDictionary *params);

/*!
 *  返回第一个字符串对象
 *
 *  @param string 输入字符串
 *
 *  @return 第一个字符串对象
 */
+ (NSString *)coco_firstNonNsNullStringWithString:(NSString *)string, ...;

/*!
 *  将URL中的参数在参数字典中保存
 *  如字符串为”param1=value1&param2=value2”, innerGlue为"=", outterGlue为"&"
 *  转换后的字典为{param1=value1; param2=value2}
 *
 *  @param innerGlue  识别键值对的符号
 *  @param outterGlue 识别不同参数的符号
 *
 *  @return 转换后的参数字典
 */
- (NSMutableDictionary *)coco_explodeToDictionaryInnerGlue:(NSString *)innerGlue
    outterGlue:(NSString *)outterGlue;

/*!
 *  将URL中的参数在参数字典中保存
 *  如字符串为”param1=value1&param2=value2”, innerGlue为"=", outterGlue为"&"
 *  转换后的字典为{param1=value1; param2=value2}
 *
 *  @param innerGlue        识别键值对的符号
 *  @param outterGlue       识别不同参数的符号
 *  @param isCompatibleMode YES时将“+”-》“%20”
 *
 *  @return 转换后的参数字典
 */
- (NSMutableDictionary *)coco_explodeToDictionaryInnerGlueUTF8Decode:(NSString *)innerGlue
    outterGlue:(NSString *)outterGlue
    isCompatibleMode:(BOOL)isCompatibleMode;

@end

/*!
 *  添加删除转义字符的工具方法。
 */
@interface NSString (CoCoTIndempotentPercentEscapes)

/*!
 *  返回一个将当前字符串中的转义字符decode的字符串
 *
 *  @return decode后的字符串
 */
- (NSString *)coco_stringByAddingPercentEscapesOnce;

/*!
 *  返回一个将当前字符串encode的字符串
 *
 *  @return encode后的字符串
 */
- (NSString *)coco_stringByReplacingPercentEscapesOnce;
@end

/*!
 *  取得UUID
 */
@interface NSString (CoCoTCreateUUID)
/*!
 *  生成并返回一个UUID字符串
 *
 *  @return UUID字符串
 */
+ (NSString *)coco_stringWithUUID;

+ (NSString *)coco_stringWithNewUUID;
@end


/*!
 *  判断当前字符串与指定子串的相关操作的工具方法。
 */
@interface NSString  (CoCoTRangeAvoidance)
/*!
 *  判断当前字符串是否包含对应子串
 *
 *  @param substring 要判断的子串
 *
 *  @return YES，包含；NO，不包含
 */
- (BOOL)coco_hasSubstring:(NSString *)substring;

/*!
 *  生成一个指定子字符串出现后的字符串，
 *  如"donganyuan" substring "an" 返回 "yuan"
 *
 *  @param substring 要判断的子串
 *
 *  @return 返回的子字符串
 */
- (NSString *)coco_substringAfterSubstring:(NSString *)substring;

/*!
 *  不考虑大小写，判断两个字符串是否相同
 *
 *  @param otherString 要比较的字符串
 *
 *  @return YES，相同；NO，不相同；
 */
- (BOOL)coco_isEqualToStringIgnoringCase:(NSString *)otherString;
@end
