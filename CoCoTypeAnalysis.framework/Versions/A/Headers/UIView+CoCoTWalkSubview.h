//
//  UIView+CoCoTWalkSubview.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  本来主要是循环遍历当前对象的subViews执行block操作，如果block参数stop返回YES则终止操作
 */
@interface UIView (CoCoTWalkSubview)
/*!
 * 循环遍历当前对象的subViews执行block操作，如果block参数stop返回YES则终止操作
 *
 *  @param walk 需要执行的block操作
 */
- (void)coco_walkSubview:(void (^)(UIView *view, BOOL *stop))walk;
@end
