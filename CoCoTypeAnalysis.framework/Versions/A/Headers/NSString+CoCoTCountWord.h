//
//  NSString+CoCoTCountWord.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  文字计数相关工具方法
 */
@interface NSString (CoCoTCountWord)

/*!
 *  返回当前字符串的字数
 *
 *  @return 字数
 */
- (int)coco_wordCount;

/*!
 *  返回 ASCII 字符的长度，如：英文占1，中文占2
 *
 *  @return 长度
 */
- (int)coco_asciiCount;

/*!
 *  判断当前字符串是否有非空字符
 *
 *  @return YES为空，NO不为空
 */
- (BOOL)coco_isEmpty;


@end
