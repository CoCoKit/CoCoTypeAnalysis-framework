//
//  UIView+CoCoTViewController.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
   source:http://stackoverflow.com/questions/1340434/get-to-uiviewcontroller-from-uiview-on-iphone
   "IMHO, this is a design flaw. The view should not need to be aware of the controller."
   But "one reason you need to allow the UIView to be aware of its UIViewController is when you have custom UIView subclasses that need to push a modal view/dialog."
 */

/*!
 * 本类主要是用来查找响应者链中第一个UIViewController对象
 */
@interface UIView (CoCoTViewController)

/*!
 *  遍历responder链，返回第一个为UIViewController类型的对象
 *  @return 返回第一个响应事件的UIViewController
 */
- (UIViewController *)coco_firstAvailableUIViewController;

/*!
 *  遍历responder链，返回第一个为UIViewController类型的对象
 *  @return 返回第一个响应事件的UIViewController
 */
- (id)coco_traverseResponderChainForUIViewController;

@end
