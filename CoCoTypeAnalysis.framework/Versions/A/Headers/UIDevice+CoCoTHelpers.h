//
//  UIDevice+CoCoTHelpers.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define CoCoKeyWindowPortraitWidth       ([UIDevice portraitWidthWithOffset:YES])
#define CoCoKeyWindowPortraitHeight      (MAX(CoCoWindow.bounds.size.width, CoCoWindow.bounds.size.height))

#define CoCoKeyWindowPortraitWidthOffset 80

#define CoCoWindow                       ((UIWindow *)([[UIApplication sharedApplication].delegate performSelector:@selector(window)]))

#define CoCoKeyWindowRealWidth \
    ({ \
        CGFloat realWidth = CoCoWindow.bounds.size.width; \
        if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 8) { \
            realWidth = CoCoWindow.bounds.size.width; \
        } else { \
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation; \
            if (UIInterfaceOrientationIsLandscape(orientation)) \
            { \
                realWidth = CoCoWindow.bounds.size.height; \
            } \
        } \
        realWidth; \
    })

#define CoCoKeyWindowRealHeight \
    ({ \
        CGFloat realHeight = CoCoWindow.bounds.size.height; \
        if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 8) { \
            realHeight = CoCoWindow.bounds.size.height; \
        } else { \
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation; \
            if (UIInterfaceOrientationIsLandscape(orientation)) \
            { \
                realHeight = CoCoWindow.bounds.size.width; \
            } \
        } \
        realHeight; \
    })

#define CoCoPortraitWidthScale           (CGFloat)CoCoKeyWindowPortraitWidth / (CGFloat)320

#define CoCoBackgroundPortraitWidthScale (CGFloat)([UIDevice portraitWidthWithOffset:NO]) / (CGFloat)320

#define CoCoRoundfScaleValue(value)   (roundf(value * CoCoHightScale))

#define CoCoHightScale \
    ({ \
        CGFloat hightScale = 1.0; \
        if ([[UIDevice currentDevice] coco_isIPhone6Plus]) { \
            hightScale = 1.15; \
        } else if ([[UIDevice currentDevice] coco_isIPhone6]) { \
            hightScale = 1.06; \
        } else if ([[UIDevice currentDevice] coco_isIPad]) { \
            hightScale = 1.25; \
        } \
        hightScale; \
    })

#define CoCoWidthScale \
    ({ \
        CGFloat widthScale = 1.0; \
        if ([[UIDevice currentDevice] coco_isIPhone6Plus]) { \
            widthScale = 1.29; \
        } else if ([[UIDevice currentDevice] coco_isIPhone6]) { \
            widthScale = 1.17; \
        } else if ([[UIDevice currentDevice] coco_isIPad]) { \
            widthScale = CoCoKeyWindowPortraitWidth / 320.f; \
        } \
        widthScale; \
    })

#define CoCoGetScaleValue(value)      roundf((value) * CoCoHightScale)
#define CoCoGetWidthScaleValue(value) roundf((value) * CoCoWidthScale)
// 根据需求universal适配时左图右文类型card文字，6&6+文字大小相同
#define CoCoGetTextFontScaleValue6And6PlusEqual(value) \
    ({ float rValue = 1.0; \
       if ([[UIDevice currentDevice] coco_isIPhone6Plus]) { rValue = roundf((value) * 1.06); } \
       else { rValue = roundf((value) * CoCoHightScale); } rValue; })
// 根据需求universal适配时左图右文类型card文字，5&6文字大小相同，6
#define CoCoGetTextFontScaleValue5And6Equal(value) \
    ({ float rValue = 1.0; \
       if ([[UIDevice currentDevice] coco_isIPhone6]) { rValue = roundf(value); } \
       else if ([[UIDevice currentDevice] coco_isIPhone6Plus]) { rValue = roundf((value) + 1); } \
       else { rValue = roundf((value) * CoCoHightScale); } rValue; })
// 根据需求universal适配部分pad需要做单独处理
#define CoCoPageCardGetScaleValueAndPadSpecialScale(value, padScale)  \
    ({ float rValue = 1.0; \
       if ([[UIDevice currentDevice] coco_isIPad] && (padScale) > CoCoHightScale) { rValue = roundf((value) * (padScale)); } \
       else { rValue = roundf((value) * CoCoHightScale); } rValue; })
// 部分需要只对pad进行Unversal适配
#define CoCoPageCardGetScaleValueJustPadSpecialScale(value)  \
    ({ float rValue = 1.0; \
       if ([[UIDevice currentDevice] coco_isIPad]) { rValue = roundf((value) * (CoCoHightScale)); } \
       else { rValue = (value); } rValue; })

/*
   对应于美工给的数值适配表
   val   : 4/5 的数值
   返回值 : 返回适配后的数值(4/5, 6, 6+)

   Todo: 之前很多地方使用了本地的宏，要统一替换
 */
#define CoCoTimelineAdaptiveValue(val) ( (CGFloat)((int)(((val) * CoCoHightScale) + 0.5f )) )




/*!
 *  主要用来判断分辨率来做布局处理，scale可能不同
 */
typedef NS_ENUM (NSUInteger, coco_ScreenType){
    coco_ScreenTypeUndefined   = 0,
    coco_ScreenTypeClassic     = 1,// 3gs及以下
    coco_ScreenTypeRetina      = 2,// 4&4s
    coco_ScreenType4InchRetina = 3,// 5&5s&5c
    coco_ScreenType6           = 4,// 6或者6+放大模式
    coco_ScreenType6Plus       = 5,// 6+
    coco_ScreenTypeIpadClassic = 6,// iPad 1,2,mini
    coco_ScreenTypeIpadRetina  = 7,// iPad 3以上,mini2以上
};

/*!
 *  本类主要用来获取与设备项目的信息以及操作
 */
@interface UIDevice (CoCoTHelpers)


+ (CGFloat)portraitWidthWithOffset:(BOOL)needOffset;

/*!
 *  判断当前屏幕类型（按分辨率分类）
 *
 *
 */
- (coco_ScreenType)screenType;

/*!
 *  判断当前手机是否为高清屏
 *
 *  @return ,返回值YES代表是高清屏，NO代表不是高清屏
 */
- (BOOL)coco_isRetinaDisplay;

/*!
 *  判断当前UIScreen的frame是否iPhone5分辨率
 *
 *
 */
- (BOOL)coco_is4InchRetinaDisplay;

/*!
 *  判断当前UIScreen的frame是否时480以上
 *
 *  使用coco_isLongScreen
 */
- (BOOL)coco_is4InchRetinaDisplay_old __attribute__((deprecated));

/*!
 *  判断当前UIScreen的frame是否时480以上
 *
 *
 */
- (BOOL)coco_isLongScreen;

/*!
 *  判断当前是否为 iPhone 6
 *
 *  @discussion 布局时请尽量避免使用此方法。好的布局应当能自动适配任何屏幕宽度，而不是针对 iPhone 6 作特殊处理。此方法通过以下条件判断：1.设备是iPhone; 2.nativeScale > 2.1; 3.屏幕高度为 736。这个判断可能在苹果发布新设备后失效
 */
- (BOOL)coco_isIPhone6;


/*!
 *  判断当前是否为 iPhone 6 Plus
 *
 *  @discussion 布局时请尽量避免使用此方法。好的布局应当能自动适配任何屏幕宽度，而不是针对 iPhone 6 Plus 作特殊处理。此方法通过以下条件判断：1.设备是iPhone; 2.nativeScale > 2.1; 3.屏幕高度为 736。这个判断可能在苹果发布新设备后失效
 *
 */
- (BOOL)coco_isIPhone6Plus;

/*!
 *  判断当前是否为 iPad
 *
 *  @discussion 布局时请尽量避免使用此方法。好的布局应当能自动适配任何屏幕宽度，而不是针对 iPhone 6 Plus 作特殊处理。此方法通过以下条件判断：1.设备是iPhone; 2.nativeScale > 2.1; 3.屏幕高度为 736。这个判断可能在苹果发布新设备后失效
 *
 */
- (BOOL)coco_isIPad;


/*!
 *  判断当前是否为 iPadClassic
 *
 *  @discussion 布局时请尽量避免使用此方法。好的布局应当能自动适配任何屏幕宽度，而不是针对 iPhone 6 Plus 作特殊处理。此方法通过以下条件判断：1.设备是iPhone; 2.nativeScale  == 1; 3.屏幕高度为 768, 宽度为1024。这个判断可能在苹果发布新设备后失效
 *
 */
- (BOOL)coco_isIPadClassic;


/*!
 *  判断当前是否为 iPadRetina
 *
 *  @discussion 布局时请尽量避免使用此方法。好的布局应当能自动适配任何屏幕宽度，而不是针对 iPhone 6 Plus 作特殊处理。此方法通过以下条件判断：1.设备是iPhone; 2.nativeScale > 2; 3.屏幕高度为 768。宽度为1024 这个判断可能在苹果发布新设备后失效
 *
 */
- (BOOL)coco_isIPadRetina;

/*!
 *  判断当前系统是不是iOS7
 *
 *  @return 返回YES表示是，NO不是
 */
- (BOOL)coco_shouldApplyiOS7Appearence;

/*!
 *  返回当前系统版本号
 */
- (int)coco_systemMainVersion;

/*!
 *  获取当前运营商信息
 */
- (NSString *)coco_carrierName;

/*!
 *  获取返回当前手机运营商国家的代码信息
 */
- (NSString *)coco_mobileCountryCode;

/*!
 *  获取移动设备的网络代码信息（以用来表示唯一一个的移动设备的网络运营商）
 */
- (NSString *)coco_mobileNetworkCode;

/*!
 *  获取当前的macAddress信息
 *
 */
- (NSString *)coco_macAddress;

/*!
 *  判断当前设备是否能打电话
 *
 *  @return YES表示可以，NO表示不可以
 */
- (BOOL)coco_canMakeCall;

/*!
 *  判断当前设备是否能发短信
 *
 *  @return YES表示可以，NO表示不可以
 */
- (BOOL)coco_canSendText;

/*!
 *  获取当前设备型号信息
 *
 */
+ (NSString *)coco_getStandardPlat;

/*!
 *  获取当前设备型号信息
 *
 */
+ (NSString *)coco_platform;

/*!
 *  返回一个当前设备型号信息对应的字符串，例如iPhone1,2对应iphone3g等
 *
 *  @param platform 系统设备型号信息，例如“iPhone1,2，iPod2,1,iPad1,1"等
 *
 *  @return 返回一个设备的通用叫法，例如“ipodtouch1,iphone4”等
 */
+ (NSString *)coco_getReturnPlat:(NSString *)platform;

/*!
 *  判断当前设备是否支持闪光灯
 *
 *  @return 返回YES表示支持，NO不支持
 */
- (BOOL)coco_hasLedLight;

/*!
 *  判断当前设备是否打开了闪光
 *
 *  @return 返回YES表示打开，NO没打开
 */
- (BOOL)coco_isLedLightOn;

/*!
 *  设置是闪光灯属性
 *
 *  @param on 返回YES打开闪光灯，NO不打开闪光灯
 */
- (void)coco_turnLedLightTo:(BOOL)on;

/*!
 *  是否是iPad布局
 *
 *  只有当工程为iPad Only或者或者Universal时候在iPad设备上才会返回YES
 *  当工程为iPhone Only在iPad设备上时，返回NO。
 *
 *
 *  @return 返回YES表示是，NO不是（是iPhone或iPod布局）
 */
- (BOOL)coco_isUIUserInterfaceIdiomPad;

/*!
 *  根据设备的能力（内存）返回不同分辨率
 *
 */
+ (NSUInteger)coco_assetMaxDimension;

// 判断设备锁屏
- (void)coco_markAsLocked;
- (void)coco_markAsUnLocked;
- (BOOL)coco_isLocked;
- (BOOL)coco_isLockedWhileRunning;


+ (BOOL)coco_isJailbroken;
+ (NSString *)coco_currentCPUArch;
+ (NSDate *)coco_dateSysctl:(NSString *)name;
+ (NSString *)coco_appUUIDWithAppPath:(NSString *)path;
+ (NSString *)coco_appAddressWithAppPath:(NSString *)path;

@end

extern CGRect CoCoConvertCGRectForDeviceScale(CGRect rect);

extern CGFloat CoCoConvertHeightForDeviceScale(CGFloat height);

extern CGFloat CoCoConvertWidthForDeviceScale(CGFloat width);
extern CGFloat CoCoConvertEdgeForGivenScale(CGFloat width, CGFloat scale);
