//
//  CoCoToolsConfig.h
//  CoCoTool
//
//  Created by 陈明 on 16-12-26.
//  Copyright (c) 2016年 CoCo. All rights reserved.
//

#import "UIDevice+CoCoTHelpers.h"

#ifdef CoCo_DEBUG
#ifndef WLog
#define WLog(fmt, ...) NSLog((@"%s [MainThread=%i] [Line %d] " fmt), __PRETTY_FUNCTION__, [NSThread isMainThread], __LINE__, ## __VA_ARGS__);
#endif
#ifndef WLog1
#define WLog1(fmt, ...)
#endif
#else
#ifdef CoCoTOOLDEBUG
#define WLog(fmt, ...) NSLog((@"%s [MainThread=%i] [Line %d] " fmt), __PRETTY_FUNCTION__, [NSThread isMainThread], __LINE__, ## __VA_ARGS__);
#define WLog1(fmt, ...)
#else
#define WLog(...)
#define WLog1(fmt, ...)
#endif
#endif


#ifndef WLogFunction
#define WLogFunction()                       WLog(@"")
#endif



#if __IPHONE_8_0 && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0
#define IF_IOS8_OR_GREATER(...)              if ([[UIDevice currentDevice] coco_systemMainVersion] >= 8) { __VA_ARGS__ }
#else
#define IF_IOS8_OR_GREATER(...)
#endif

#if __IPHONE_7_0 && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0
#define IF_IOS7_OR_GREATER(...)              if ([[UIDevice currentDevice] coco_systemMainVersion] >= 7) { __VA_ARGS__ }
#else
#define IF_IOS7_OR_GREATER(...)
#endif

#if __IPHONE_6_0 && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0
#define IF_IOS6_OR_GREATER(...)              if ([[UIDevice currentDevice] coco_systemMainVersion] >= 6.0) { __VA_ARGS__ }
#else
#define IF_IOS6_OR_GREATER(...)
#endif
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 50000
#define IF_IOS5_OR_GREATER(...)              if ([[UIDevice currentDevice] coco_systemMainVersion] >= 5.0) { __VA_ARGS__ }
#else
#define IF_IOS5_OR_GREATER(...)
#endif
// 辅助方法

#define CoCoFunctionBeginOS(version, method) if ([[UIDevice currentDevice] coco_systemMainVersion] >= version) { method }
#define CoCoFunctionBelowOS(version, method) if ([[UIDevice currentDevice] coco_systemMainVersion] <= version) { method }

#define CoCoAvalibleOS(os_version)           ([[UIDevice currentDevice] coco_systemMainVersion] >= os_version)
#define CoCoAvalibleRetinaDevice      ([[UIDevice currentDevice] coco_isRetinaDisplay])
#define CoCoAvalible4InchRetinaDevice ([[UIDevice currentDevice] coco_isLongScreen])
